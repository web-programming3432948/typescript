let w: unknown = 1;
w = "string";
w = {
    ruanANonExistentMethod: () => {
        console.log("I think therefore I am");
    }
} as { ruanANonExistentMethod: () => void }

if(typeof w === 'object' && w!== null){
    (w as { ruanANonExistentMethod: Function}).ruanANonExistentMethod();
}